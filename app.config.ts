export default defineAppConfig({
  title: "ChEBI",
  githubIssueUrl: "https://github.com/ebi-chebi/ChEBI/issues",
  ftpBaseUrl: "https://ftp.ebi.ac.uk/pub/databases/chebi-2/",
  maximumSearchResults: 20000,
});
