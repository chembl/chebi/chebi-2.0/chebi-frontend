<template>
  <v-container>
    <h1 class="vf-text vf-text-heading--2">About ChEBI</h1>
    <h3 class="vf-text vf-text-heading--3">Introduction</h3>
    <p class="vf-section-header__text">
      <b>ChEBI</b> is an open-access database and ontology of chemical entities.
      The term ‘chemical entity’ in ChEBI refers to any constitutionally or
      isotopically distinct atom, molecule, ion, ion pair, radical, radical ion,
      complex, conformer, etc. The term also includes groups (part- molecular
      entities), chemical substances, and classes of molecular entities. The
      chemical entities in ChEBI are either naturally occurring molecules or
      synthetic compounds used to intervene in the processes of living
      organisms. Macromolecules directly encoded by the genome (e.g. nucleic
      acids, proteins and peptides derived from proteins by cleavage) are not as
      a rule included in ChEBI. The >195,000 entries in the database can be
      queried using text and structure searches (e.g. by name, molecular
      formula, InChI, or SMILES). For each entity ChEBI may include a wealth of
      other information such as literature citations, cross-references to other
      databases, and species data.
    </p>
    <p class="vf-section-header__text">
      ChEBI uses the nomenclature, symbolism and terminology endorsed by the
      International Union of Pure and Applied Chemistry (<NuxtLink
        external
        to="https://iupac.org/"
        >IUPAC</NuxtLink
      >) and the Nomenclature Committee of the International Union of
      Biochemistry and Molecular Biology (<NuxtLink
        external
        to="https://iubmb.org/"
        >NC-IUBMB</NuxtLink
      >). ChEBI also incorporates an ontological classification, whereby the
      relationships between chemical entities or classes of entities and their
      parents and/or children are defined; this enables queries based for
      example on chemical class and role.
    </p>
    <br />
    <h3 class="vf-text vf-text-heading--3">How is ChEBI used?</h3>
    <p class="vf-section-header__text">
      ChEBI is very widely used as a small molecule reference database by
      multiple resources worldwide and has steadily evolved and grown over the
      years. It is now a crucial part of the global biosciences and informatics
      infrastructure. In 2017, ChEBI was designated an ELIXIR core data resource
      and in 2022, it was selected as a Global core biodata resource in
      recognition of its fundamental importance to the wider biological and life
      sciences community. The list of databases that use ChEBI’s data is long
      and varied and includes, amongst others:
      <a target="_blank" href="https://www.rhea-db.org/" class="vf-link">Rhea</a
      >,
      <a
        target="_blank"
        href="https://www.ebi.ac.uk/metabolights/"
        class="vf-link"
        >MetaboLights</a
      >,
      <a target="_blank" href="https://www.uniprot.org/" class="vf-link"
        >UniProt</a
      >,
      <a target="_blank" href="https://geneontology.org/" class="vf-link">GO</a
      >,
      <a target="_blank" href="https://www.iedb.org/" class="vf-link">IEDB</a>,
      <a target="_blank" href="https://reactome.org/" class="vf-link"
        >Reactome</a
      >,
      <a
        target="_blank"
        href="https://pubchem.ncbi.nlm.nih.gov/"
        class="vf-link"
        >PubChem</a
      >,
      <a target="_blank" href="https://www.ebi.ac.uk/biomodels/" class="vf-link"
        >BioModels</a
      >,
      <a target="_blank" href="https://www.ebi.ac.uk/intact/" class="vf-link"
        >IntAct</a
      >,
      <a target="_blank" href="https://www.swisslipids.org/" class="vf-link"
        >SwissLipids</a
      >,
      <a target="_blank" href="https://metaspace2020.eu/" class="vf-link"
        >Metaspace</a
      >
      and the
      <a target="_blank" href="https://www.ebi.ac.uk/ols4" class="vf-link"
        >Ontology Lookup Service</a
      >. For some of these resources, ChEBI is the sole source of accurate small
      molecule structural information linked to a stable identifier. Other
      resources use and import ChEBI’s unique chemical ontology, which when
      combined with other ontology terms provides powerful capabilities for data
      integration, hypothesis generation and reasoning.
    </p>
    <br />
    <h3 class="vf-text vf-text-heading--3">Sources</h3>
    <p class="vf-section-header__text">
      ChEBI was first released in 2004. To create ChEBI, data from a number of
      sources were incorporated and subjected to merging procedures to eliminate
      redundancy.<br />Four of the main sources from which the data was derived
      are:
    </p>
    <br />
    <ul class="vf-list--unordered vf-section-header__text">
      <li class="">
        <a target="_blank" href="https://www.ebi.ac.uk/intenz" class="vf-link"
          >IntEnz</a
        >
        – the <b>Int</b>egrated relational <b>Enz</b>yme database and official
        version of the Enzyme Nomenclature, the recommendations of the NC-IUBMB
        on the Nomenclature and Classification of Enzyme-Catalysed Reactions.
        The database has now been deprecated.
      </li>
      <li class="">
        <a
          target="_blank"
          href="https://www.genome.jp/kegg/compound/"
          class="vf-link"
          >KEGG COMPOUND</a
        >
        – one of the four original databases that contains a collection of small
        molecules, biopolymers, and other chemical substances that are relevant
        to biological systems. It was introduced at the start of the KEGG
        project in 1995.
      </li>
      <li class="">
        <a
          target="_blank"
          href="https://www.ebi.ac.uk/pdbe-srv/pdbechem/"
          class="vf-link"
          >PDBeChem</a
        >
        – A dictionary of chemical components (ligands, small molecules and
        monomers) referred to in PDB entries and maintained by wwPDB.
      </li>
      <li class="">
        <a target="_blank" href="https://www.ebi.ac.uk/chembl/" class="vf-link"
          >ChEMBL</a
        >
        – A manually curated database of bioactive molecules with drug-like
        properties.
      </li>
    </ul>
    <p class="vf-section-header__text">
      Over the past several years, data from other sources have also been
      included into ChEBI including the
      <a target="_blank" href="https://hmdb.ca/" class="vf-link">HMDB</a>,
      <a target="_blank" href="https://lincsproject.org/" class="vf-link"
        >LINCS</a
      >,
      <a target="_blank" href="https://drugcentral.org/" class="vf-link"
        >DrugCentral</a
      >,
      <a
        target="_blank"
        href="https://www.metabolomicsworkbench.org/"
        class="vf-link"
        >Metabolomics Workbench</a
      >
      and
      <a target="_blank" href="https://www.glygen.org/" class="vf-link"
        >GlyGen databases</a
      >. Apart from this, the main source of the data today are our submitters
      which include individual users and major databases such as
      <a
        target="_blank"
        href="https://www.ebi.ac.uk/metabolights/"
        class="vf-link"
        >MetaboLights</a
      >
      and
      <a target="_blank" href="https://www.rhea-db.org/" class="vf-link"
        >Rhea</a
      >
      who submit new data via the submissions tool.
    </p>
    <br />
    <h3 class="vf-text vf-text-heading--3">Data</h3>
    <p class="vf-section-header__text">
      Each ChEBI entry may include some or all of the following data fields:
    </p>
    <ul class="vf-list--unordered vf-section-header__text">
      <li class="">
        ChEBI Identifer – A unique and stable identifer assigned by ChEBI
      </li>
      <li class="">
        ChEBI Name – the name recommended for use in biological databases
      </li>
      <li class="">
        ChEBI ASCII Name – the ChEBI name with any special characters rendered
        in ASCII format
      </li>
      <li class="">
        Star rating – A rating based on the level of manual curation
      </li>
      <li class="">
        Definition – A logical and natural language definition of the term
      </li>
      <li class="">
        Last Modified – The date in which the entry was last modified
      </li>
      <li class="">
        Submitter – The name of the user or resource who submitted the entry
      </li>
      <li class="">
        Downloads – The option to download the data of the entry in several file
        formats
      </li>
      <li class="">
        Structure – graphical representation(s) of the molecular structure and
        associated molfile(s), IUPAC International Chemical Identifier (InChI),
        SMILES strings, and WURCS (for carbohydrates).
      </li>
      <li class="">Formula – Molecular formula</li>
      <li class="">Net Charge</li>
      <li class="">Average Mass</li>
      <li class="">Monoisotopic Mass</li>
      <li class="">
        Species of Metabolite – A table showing the species where the chemical
        entity is found
      </li>
      <li class="">ChEBI Ontology</li>
      <ul class="">
        <li class="">Outgoing and incoming ontology relations</li>
        <li class="">
          A tree view which shows the position of the entry within the ChEBI
          Ontology
        </li>
      </ul>
      <li class="">
        IUPAC Name – name(s) generated according to recommendations of IUPAC
      </li>
      <li class="">
        INN – International Nonproprietary Name, also known as generic name,
        assigned by the World Health Organization (WHO)
      </li>
      <li class="">
        Synonyms – other names together with an indication of their source
      </li>
      <li class="">Brand Name – a trade or proprietary name</li>
      <li class="">
        Database Links – manually curated cross-references to other
        non-proprietary databases
      </li>
      <li class="">
        Registry Number – CAS Registry Number, Reaxys Registry Number, Gmelin
        Registry Number (if available)
      </li>
      <li class="">
        Citations – Publications which cite the entity along with hyperlinks to
        the articles.
      </li>
    </ul>
    <p class="vf-section-header__text">
      In addition, a separate page called 'Automatic Xrefs' contains
      automatically generated cross-references to a number of biological and
      chemical databases.
    </p>

    <br />
    <h3 class="vf-text vf-text-heading--3">Licensing</h3>
    <p class="vf-section-header__text">
      All data in the ChEBI database is non-proprietary or is derived from a
      non-proprietary source. It is thus freely accessible and available to
      anyone. Each data item is fully traceable and explicitly referenced to the
      original source.
    </p>

    <p class="vf-section-header__text">
      The data on this website is available under the Creative Commons License
      (<a
        target="_blank"
        href="https://creativecommons.org/licenses/by/4.0/deed.en"
        class="vf-link"
        >CC BY 4.0</a
      >), and governed by
      <a
        target="_blank"
        href="https://www.ebi.ac.uk/about/terms-of-use"
        class="vf-link"
        >EMBL-EBI’s terms of use</a
      >
      and
      <a
        target="_blank"
        href="https://www.ebi.ac.uk/long-term-data-preservation"
        class="vf-link"
        >Long-term data preservation</a
      >
      policy.
    </p>

    <br />
    <h3 class="vf-text vf-text-heading--3">Publications</h3>
    <p class="vf-section-header__text">To cite ChEBI:</p>
    <p class="vf-section-header__text">
      Hastings J, Owen G, Dekker A, Ennis M, Kale N, Muthukrishnan V, Turner S,
      Swainston N, Mendes P, Steinbeck C. (2016). ChEBI in 2016: Improved
      services and an expanding collection of metabolites.
      <a
        target="_blank"
        href="https://europepmc.org/article/MED/26467479"
        class="vf-link"
        ><i>Nucleic Acids Res.</i></a
      >
    </p>
    <p class="vf-section-header__text">Other publications:</p>
    <ul class="vf-list--unordered vf-section-header__text">
      <li class="">
        Hastings, J., de Matos, P., Dekker, A., Ennis, M., Harsha, B., Kale, N.,
        Muthukrishnan, V., Owen, G., Turner, S., Williams, M., and Steinbeck, C.
        (2013) The ChEBI reference database and ontology for biologically
        relevant chemistry: enhancements for 2013.
        <a
          target="_blank"
          href="https://dx.doi.org/10.1093/nar/gks1146"
          class="vf-link"
          ><i>Nucleic Acids Res.</i></a
        >
      </li>
      <li class="">
        de Matos, P., Alcantara, R., Dekker, A., Ennis, M., Hastings, J., Haug,
        K., Spiteri, I., Turner, S., and Steinbeck, C. (2010) Chemical entities
        of biological interest: an update.
        <a
          target="_blank"
          href="https://academic.oup.com/nar/article/38/suppl_1/D249/3112238"
          class="vf-link"
          ><i>Nucleic Acids Res.</i></a
        >
      </li>
      <li class="">
        Degtyarenko, K., Hastings, J., de Matos, P., and Ennis, M. (2009).
        ChEBI: an open bioinformatics and cheminformatics resource.
        <a
          target="_blank"
          href="https://dx.doi.org/10.1002/0471250953.bi1409s26"
          class="vf-link"
        >
          <i>Current protocols in bioinformatics</i> / editoral board, Andreas
          D. Baxevanis ... [et al.], Chapter 14.</a
        >
      </li>
      <li class="">
        Degtyarenko, K., de Matos, P., Ennis, M., Hastings, J., Zbinden, M.,
        McNaught, A., Alcántara, R., Darsow, M., Guedj, M. and Ashburner, M.
        (2008) ChEBI: a database and ontology for chemical entities of
        biological interest.
        <a
          target="_blank"
          href="https://dx.doi.org/10.1093/nar/gkm791"
          class="vf-link"
        >
          <i>Nucleic Acids Res.</i> 36, D344–D350.</a
        >
      </li>
    </ul>

    <br />
    <h3 class="vf-text vf-text-heading--3">Staying in Touch</h3>
    <p class="vf-section-header__text">
      Feel free to
      <NuxtLink to="contact" target="_blank">contact us</NuxtLink> if you find
      any issues in the data, or need help with anything. To keep up to date
      with ChEBI news and data releases,
      <a href="https://x.com/chebit" target="_blank">follow us on X</a>.
    </p>

    <br />
    <h3 class="vf-text vf-text-heading--3">Acknowledgements</h3>
    <p class="vf-section-header__text">
      ChEBI is currently funded by the member states of EMBL and the BBSRC,
      grant agreement number BB/V018566/1 within the "Bioinformatics and
      biological resources" fund.
    </p>
    <p class="vf-section-header__text">Previous funding:</p>

    <ul class="vf-list--unordered vf-section-header__text">
      <li class="">ELIXIR staff exchange grant (EBI-2020-SEP12).</li>
      <li class="">
        BBSRC, grant agreement number BB/K019783/1 within the "Bioinformatics
        and biological resources" fund.
      </li>
      <li class="">
        European Commission under SLING, grant agreement number 226073
        (Integrating Activity) within Research Infrastructures of the FP7
        Capacities Specific Programme.
      </li>
      <li class="">
        BBSRC, grant agreement number BB/G022747/1 within the "Bioinformatics
        and biological resources" fund.
      </li>
      <li class="">
        European Commission under FELICS, contract number 021902 (RII3) within
        the Research Infrastructure Action of the FP6 "Structuring the European
        Research Area" Programme.
      </li>
      <li class="">
        BioBabel grant (no. QLRT-CT-2001-00981) of the European Commission.
      </li>
    </ul>
    <br />
    <p class="vf-section-header__text">
      We would like to acknowledge the following software support:
    </p>
    <br />
    <div class="acknowledgments">
      <div v-for="(logo, index) in logos" :key="index" class="logo-item">
        <img :src="logo.url" :alt="logo.name" class="logo-image" />
      </div>
    </div>
  </v-container>
</template>

<style scoped>
ul {
  list-style-position: inside;
}
ul ul {
  padding-left: 1em;
}

.acknowledgments {
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  gap: 20px;
}

.logo-item {
  text-align: center;
}

.logo-image {
  height: 35px;
}
</style>
<script setup lang="ts">
const runtimeConfig = useRuntimeConfig();
const baseUrl = runtimeConfig.app.baseURL;
const logos = [
  { name: "Django", url: baseUrl + "other_logos/django.svg" },
  { name: "ElasticSearch", url: baseUrl + "other_logos/elastic.svg" },
  { name: "GitLab", url: baseUrl + "other_logos/gitlab.svg" },
  { name: "GitHub", url: baseUrl + "other_logos/GitHub.png" },
  { name: "Python", url: baseUrl + "other_logos/python.svg" },
  { name: "Robot OBO", url: baseUrl + "other_logos/robot.png" },
  { name: "ClassyFire", url: baseUrl + "other_logos/classyfire.png" },
  { name: "Docker", url: baseUrl + "other_logos/docker.svg" },
  { name: "Kubernetes", url: baseUrl + "other_logos/k8.svg" },
  { name: "ChemAxon", url: baseUrl + "other_logos/chemaxon.png" },
  { name: "Nuxt", url: baseUrl + "other_logos/nuxt.svg" },
  { name: "Luigi", url: baseUrl + "other_logos/luigi.png" },
  { name: "Pandas", url: baseUrl + "other_logos/pandas.svg" },
  { name: "dbt", url: baseUrl + "other_logos/dbt.svg" },
  { name: "VueJS", url: baseUrl + "other_logos/vue.svg" },
  { name: "Postgres", url: baseUrl + "other_logos/postgres.png" },
  { name: "RdKit", url: baseUrl + "other_logos/rdkit.png" },
  { name: "Pronto", url: baseUrl + "other_logos/pronto.png" },
];
</script>
