FROM node:18-alpine

# Set environment variables
ENV NUXT_PUBLIC_GTAG_ID=xyz
ENV NUXT_APP_BASE_URL=api_url

WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build
EXPOSE 3000
ENTRYPOINT [ "node", ".output/server/index.mjs" ]
