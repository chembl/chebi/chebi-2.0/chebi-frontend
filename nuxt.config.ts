import { defineNuxtConfig } from "nuxt/config";

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  runtimeConfig: {
    public: {
      apiBaseUrl: "",
      mediaBaseURL: "",
    },
  },
  modules: [
    "@vueuse/nuxt",
    "@pinia/nuxt",
    "@pinia-plugin-persistedstate/nuxt",
    "nuxt-gtag",
    "@nuxtjs/eslint-module",
    "nuxt-lodash",
  ],
  gtag: {
    id: "",
  },
  eslint: {
    /* module options */
  },
  css: [
    "vuetify/styles",
    "@mdi/font/css/materialdesignicons.min.css",
    "@/assets/styles/main.css",
    "@/assets/styles/variables.scss",
    "@/assets/styles/vuetify_custom.sass",
  ],
  build: {
    transpile: ["vuetify"],
  },
  vite: {
    define: {
      "process.env.DEBUG": true,
    },
  },
  app: {
    head: {
      bodyAttrs: {
        class: "",
      },
      script: [
        {
          src: "https://ebi.emblstatic.net/web_guidelines/EBI-Framework/v1.4/js/script.js",
        },
      ],
      link: [
        // EBI generic header footer
        // Revisit if this is the right place for these styles
        {
          rel: "stylesheet",
          href: "https://assets.emblstatic.net/vf/v2.5.13/css/styles.css",
        },
      ],
    },
  },
  typescript: {
    typeCheck: true,
    strict: true,
  },
});
