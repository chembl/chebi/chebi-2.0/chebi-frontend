import type {
  Term,
  Range,
  Ontology,
  Keyword,
} from "~/types/advancedSearchQuery";
import type {
  APIRangeType,
  APIOntologyType,
  APITermType,
  RangeSpecification,
  TermSpecification,
  OntologySpecification,
  KeywordSpecification,
  APIKeywordType,
  APISpeciesKeywordType,
} from "~/types/APIAdvancedSearchQuery";
import type { AdvancedSearchQuery } from "~/types/advancedSearchQuery";
import type { APIAdvancedSearchQuery } from "~/types/APIAdvancedSearchQuery";
import { APIAdvancedSearchModel } from "~/types/APIAdvancedSearchModel";

function _transformTermQuery(termQuery: Term[]): TermSpecification {
  const apiTermQuery: TermSpecification = {
    and_specification: [],
    or_specification: [],
    but_not_specification: [],
  };
  for (const term of termQuery) {
    const { operator } = term;
    const apiTerm: APITermType = { term: term.term };
    if (apiTerm.term) {
      if (operator === "AND") {
        apiTermQuery.and_specification.push(apiTerm);
      } else if (operator === "OR") {
        apiTermQuery.or_specification.push(apiTerm);
      } else if (operator === "BUT NOT") {
        apiTermQuery.but_not_specification.push(apiTerm);
      }
    }
  }
  return apiTermQuery;
}

function _transformRangeQuery(rangeQuery: Range[]): RangeSpecification {
  const apiRangeQuery: RangeSpecification = {
    and_specification: [],
    or_specification: [],
    but_not_specification: [],
  };
  for (const range_item of rangeQuery) {
    const { operator } = range_item;
    const apiTerm: APIRangeType = {
      init_range: range_item.init_range,
      final_range: range_item.final_range,
    };
    // TODO: Allow only one range as well
    if (apiTerm.init_range && apiTerm.final_range) {
      if (operator === "AND") {
        apiRangeQuery.and_specification.push(apiTerm);
      } else if (operator === "OR") {
        apiRangeQuery.or_specification.push(apiTerm);
      } else if (operator === "BUT NOT") {
        apiRangeQuery.but_not_specification.push(apiTerm);
      }
    }
  }
  return apiRangeQuery;
}

function _transformOntologyQuery(
  ontologyQuery: Ontology[],
): OntologySpecification {
  const apiOntologyQuery: OntologySpecification = {
    and_specification: [],
    or_specification: [],
    but_not_specification: [],
  };
  for (const ontology_item of ontologyQuery) {
    const { operator } = ontology_item;
    const apiTerm: APIOntologyType = {
      entity: ontology_item.entity,
      relation: ontology_item.relation,
    };
    if (apiTerm.entity && apiTerm.relation) {
      if (operator === "AND") {
        apiOntologyQuery.and_specification.push(apiTerm);
      } else if (operator === "OR") {
        apiOntologyQuery.or_specification.push(apiTerm);
      } else if (operator === "BUT NOT") {
        apiOntologyQuery.but_not_specification.push(apiTerm);
      }
    }
  }
  return apiOntologyQuery;
}

function _transformKeywordQuery(keywordQuery: Keyword[]): KeywordSpecification {
  const apiTextQuery: KeywordSpecification = {
    and_specification: [],
    or_specification: [],
    but_not_specification: [],
  };
  const operatorMapping = {
    AND: apiTextQuery.and_specification,
    OR: apiTextQuery.or_specification,
    "BUT NOT": apiTextQuery.but_not_specification,
  };
  for (const keyword_item of keywordQuery) {
    if (keyword_item.keyword && keyword_item.category) {
      const { operator } = keyword_item as {
        operator: "AND" | "OR" | "BUT NOT";
      };
      let apiTerm: APISpeciesKeywordType | APIKeywordType;
      if (keyword_item.category == "IUPAC_NAME") {
        apiTerm = {
          texts: [keyword_item.keyword, "IUPAC_NAME"],
          categories: ["synonyms.name", "synonyms.type"],
        };
      } else if (keyword_item.category == "species") {
        apiTerm = {
          texts: [keyword_item.keyword],
          categories: ["compound_origins.all"],
        };
      } else {
        apiTerm = {
          text: keyword_item.keyword,
          category: keyword_item.category,
        };
      }
      operatorMapping[operator].push(apiTerm);
    }
  }
  return apiTextQuery;
}

export function transformSearchQuery(
  data: AdvancedSearchQuery,
): APIAdvancedSearchQuery {
  const transformedData: APIAdvancedSearchQuery = APIAdvancedSearchModel;

  // Term Specifications
  transformedData.formula_specification = _transformTermQuery(
    data.formula_specification,
  );
  transformedData.database_name_specification = _transformTermQuery(
    data.database_name_specification,
  );

  // Range Specifications
  transformedData.charge_specification = _transformRangeQuery(
    data.charge_specification,
  );
  transformedData.mass_specification = _transformRangeQuery(
    data.mass_specification,
  );
  transformedData.monoisotopicmass_specification = _transformRangeQuery(
    data.monoisotopicmass_specification,
  );

  // Ontology Specification
  transformedData.ontology_specification = _transformOntologyQuery(
    data.ontology_specification,
  );

  // Keyword Specification
  transformedData.text_search_specification = _transformKeywordQuery(
    data.keyword_specification,
  );

  if (data.structure_search?.mol) {
    transformedData.structure_search = {
      structure: data.structure_search.mol,
      type: data.structure_search.type,
    };

    if (data.structure_search.type === "similarity") {
      transformedData.structure_search.similarity =
        data.structure_search.similarity / 100;
    }
  }

  return transformedData;
}
