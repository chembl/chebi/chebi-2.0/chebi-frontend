
# ChEBI Frontend

Nuxt3 based frontend interface for ChEBI2.0. Follow this guide to setup the project and to contribute to it.


## Run Locally

This project can be setup locally either via Docker or npm/yarn. Follow these common steps:

- Clone the repository

- Create a new file `.env` and copy contents of `env.template` to it.

#### Run via Docker

- Build the Docker image locally
    ```bash
    docker build -t chebi-frontend .
    ```
- Run the container
    ```bash
    docker run -p 3000:3000 --env-file .env chebi-frontend
    ```

#### Run via npm

This project can also be setup using npm/yarn

- Install the dependencies
    ```bash
    npm install
    ```

- Run the development server

    ```bash
    npm run dev
    ```


Frontend should be all set and running at localhost:3000

## Deployment

Auto deployment is configured via GitLab CI. Any changes pushed to main branch trigger the pipeline which builds the container and deploys it.

Just incase, if a manual deployment needs to be trigerred locally follow these steps.

#### 1- Build Docker

`docker build --platform linux/amd64 -t dockerhub.ebi.ac.uk/chembl/chebi/chebi-2.0/chebi-frontend .`

#### 2- Push container

`docker push dockerhub.ebi.ac.uk/chembl/chebi/chebi-2.0/chebi-frontend`

#### 3- Deploy to K8

`kubectl apply -f k8s-deployment.yaml -n chebi-staging`


## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

As a quick start, copy contents of `env.template` and paste it in `.env`. Some environment variables are set with a default value, they will most likely to remain same for local development. If you're running backend server locally as explained, then following two environment variables should be:
```
NUXT_PUBLIC_API_BASE_URL=http://localhost:8000/api
NUXT_PUBLIC_MEDIA_BASE_URL=http://localhost:8000
```
Or it should point to staging server with `NUXT_PUBLIC_API_BASE_URL` having /api at the end.

`NUXT_APP_BASE_URL` and `NUXT_PUBLIC_GTAG_ID` are not needed for local development.

#### For running in kubernetes, configure k8 secrets config for the keys provided in env.template

## Contributing
Please make sure you run following linting command before adding any commit
```
npm run lint
```
in order to adhere to the project's style guidelines.
