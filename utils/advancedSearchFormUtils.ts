// Common utilities used at Advanced Search Form

export const advancedSearchQueryOperators = ["AND", "OR", "BUT NOT"] as const;
export const advancedSearchDefaultQueryOperator = "AND";
export const rules = {
  required: (value: any) => !!value || "Field is required",
  maxLength: (max: number) => (value: string) =>
    (value && value.length <= max) || `Max length is ${max}`,
  onlyNumbers: (value: string | null) => {
    const numberRegex = /^-?\d*\.?\d+$/;
    if (value && !numberRegex.test(String(value))) {
      return "Please enter a valid number";
    }
    return true;
  },
  validEmail: (value: any): boolean | string => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(value) || "Please enter a valid email address.";
  },
};

export function advancedSearchFieldRules(
  queries_length: number,
  default_rules: Array<(value: string | null) => true | string> = [],
) {
  if (queries_length === 1) {
    // By default, if there's only one query then we don't enforce the rule as user might not want to use that filter
    return default_rules;
  } else {
    return [rules.required, ...default_rules];
  }
}

export function getCookie(name: string) {
  // Borrowed from Django docs
  // https://docs.djangoproject.com/en/5.1/howto/csrf/#using-csrf
  let cookieValue = null;
  if (document.cookie && document.cookie !== "") {
    const cookies = document.cookie.split(";");
    for (let i = 0; i < cookies.length; i++) {
      const cookie = cookies[i].trim();
      if (cookie.substring(0, name.length + 1) === name + "=") {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}
