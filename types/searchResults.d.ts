export interface SearchResult {
  results: {
    _id: number;
    _score: number;
    _source: {
      charge?: number;
      chebi_accession: string;
      mass?: number;
      name: string;
      formula?: string;
      default_structure?: string;
      stars: number;
      monoisotopicmass?: number;
      ascii_name: string;
      definition?: string;
    };
  }[];
}
