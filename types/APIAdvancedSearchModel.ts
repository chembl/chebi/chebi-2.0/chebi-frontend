import type { APIAdvancedSearchQuery } from "~/types/APIAdvancedSearchQuery";

export const APIAdvancedSearchModel: APIAdvancedSearchQuery = {
  formula_specification: {
    and_specification: [],
    or_specification: [],
    but_not_specification: [],
  },
  ontology_specification: {
    and_specification: [],
    or_specification: [],
    but_not_specification: [],
  },
  mass_specification: {
    and_specification: [],
    or_specification: [],
    but_not_specification: [],
  },
  monoisotopicmass_specification: {
    and_specification: [],
    or_specification: [],
    but_not_specification: [],
  },
  charge_specification: {
    and_specification: [],
    or_specification: [],
    but_not_specification: [],
  },
  database_name_specification: {
    and_specification: [],
    or_specification: [],
    but_not_specification: [],
  },
};
