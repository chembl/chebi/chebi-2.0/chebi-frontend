import type { AdvancedSearchQuery } from "~/types/advancedSearchQuery";
export const AdvancedSearchModel: AdvancedSearchQuery = {
  formula_specification: [
    {
      term: null,
      operator: advancedSearchDefaultQueryOperator,
    },
  ],
  ontology_specification: [
    {
      relation: null,
      entity: null,
      operator: advancedSearchDefaultQueryOperator,
    },
  ],
  mass_specification: [
    {
      init_range: null,
      final_range: null,
      operator: advancedSearchDefaultQueryOperator,
    },
  ],
  monoisotopicmass_specification: [
    {
      init_range: null,
      final_range: null,
      operator: advancedSearchDefaultQueryOperator,
    },
  ],
  charge_specification: [
    {
      init_range: null,
      final_range: null,
      operator: advancedSearchDefaultQueryOperator,
    },
  ],
  database_name_specification: [
    {
      term: null,
      operator: advancedSearchDefaultQueryOperator,
    },
  ],
  keyword_specification: [
    {
      keyword: null,
      category: null,
      operator: advancedSearchDefaultQueryOperator,
    },
  ],
  threeStarOnly: false,
  structure_search: {
    mol: null,
    type: "connectivity",
    similarity: 50,
  },
};
