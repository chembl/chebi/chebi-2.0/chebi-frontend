import { advancedSearchQueryOperators } from "~/utils/advancedSearchFormUtils";

export interface AdvancedSearchQuery {
  formula_specification: Term[];
  ontology_specification: Ontology[];
  mass_specification: Range[];
  monoisotopicmass_specification: Range[];
  charge_specification: Range[];
  database_name_specification: Term[];
  keyword_specification: Keyword[];
  threeStarOnly: boolean;
  structure_search?: {
    mol: null | string;
    type: "similarity" | "connectivity" | "substructure";
    similarity: number;
  };
}

interface Ontology {
  relation: string | null;
  entity: string | null;
  operator?: advancedSearchQueryOperators;
}

interface Keyword {
  keyword: string | null;
  category: string | null;
  operator?: advancedSearchQueryOperators;
}

interface Term {
  term: string | null;
  operator?: advancedSearchQueryOperators;
}

interface Range {
  init_range: number | null;
  final_range: number | null;
  operator?: advancedSearchQueryOperators;
}
