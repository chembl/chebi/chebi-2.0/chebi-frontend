export interface APIAdvancedSearchQuery {
  formula_specification: TermSpecification;
  ontology_specification?: OntologySpecification;
  mass_specification?: RangeSpecification;
  monoisotopicmass_specification?: RangeSpecification;
  charge_specification?: RangeSpecification;
  database_name_specification?: TermSpecification;
  text_search_specification?: KeywordSpecification;
  structure_search?: {
    structure: string;
    type: string;
    similarity?: number;
  };
}

interface OntologySpecification {
  and_specification: APIOntologyType[];
  or_specification: APIOntologyType[];
  but_not_specification: APIOntologyType[];
}

interface TermSpecification {
  and_specification: APITermType[];
  or_specification: APITermType[];
  but_not_specification: APITermType[];
}

interface RangeSpecification {
  and_specification: APIRangeType[];
  or_specification: APIRangeType[];
  but_not_specification: APIRangeType[];
}

interface KeywordSpecification {
  and_specification: (APIKeywordType | APISpeciesKeywordType)[];
  or_specification: (APIKeywordType | APISpeciesKeywordType)[];
  but_not_specification: (APIKeywordType | APISpeciesKeywordType)[];
}

interface APIOntologyType {
  relation: string | null;
  entity: string | null;
}

interface APITermType {
  term: string | null;
}

interface APIRangeType {
  init_range: number | null;
  final_range: number | null;
}

interface APIKeywordType {
  text: string | null;
  category: string | null;
}

interface APISpeciesKeywordType {
  texts: string[] | null;
  categories: string[] | null;
}
