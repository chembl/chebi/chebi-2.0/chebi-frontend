export interface ChemicalDataType {
  status: number;
  formula: string;
  charge: number;
  mass: string;
  monoisotopic_mass: string;
}

export interface DefaultStructureType {
  id: number;
  smiles: string;
  standard_inchi: string;
  standard_inchi_key: string;
  wurcs?: string;
  is_r_group: boolean;
}

export interface NameType {
  [key: string]: {
    name: string;
    type: string;
    status: string;
    source: number;
    ascii_name: string;
    url_abbr: number | null;
    adapted: boolean;
    language_code: string;
  }[];
}

export interface OntologyRelationType {
  relation_type: string;
  init_id: number;
  final_id: number;
  init_name: string;
  final_name: string;
}

export interface RolesClassificationType {
  application: boolean;
  chemical_role: boolean;
  chebi_accession: boolean;
  name: string;
  definition: string;
  id: number;
  biological_role: boolean;
}

export interface DatabaseAccessionItemType {
  id: number;
  accession_number: string;
  source_name: string;
  url: string;
  prefix: string;
}

export interface CompoundOriginType {
  [speciesName: string]: {
    species_text: string;
    species_accession: string;
    species_accession_url: string;
    species_accession_prefix: string;
    origins: {
      species_text: string;
      species_accession: string;
      component_text: string | null;
      component_accession: string | null;
      strain_text: string | null;
      strain_accession: string | null;
      source: string;
      source_accession: string;
      comments: string | null;
      species_accession_url: string;
      source_accession_url: string;
      species_accession_prefix: string;
      component_accession_url: string;
    }[];
  };
}

export interface CompoundType {
  id: number;
  name: string;
  ascii_name: string;
  chebi_accession: string;
  created_by: [string] | [];
  stars: number;
  definition: string;
  modified_on: string;
  default_structure: DefaultStructureType;
  chemical_data: ChemicalDataType;
  names: NameType[];
  secondary_ids?: [string] | [];
  compound_origins: CompoundOriginType[];
  ontology_relations: {
    incoming_relations: OntologyRelationType[];
    outgoing_relations: OntologyRelationType[];
  };
  database_accessions: {
    MANUAL_X_REF?: DatabaseAccessionItemType[];
    REGISTRY_NUMBER?: DatabaseAccessionItemType[];
    CITATION?: DatabaseAccessionItemType[];
    CAS?: DatabaseAccessionItemType[];
  };
  roles_classification: RolesClassificationType[];
  is_released: boolean;
}
