import { defineStore } from "pinia";
import type { SearchResult } from "~/types/searchResults";
import { transformSearchQuery } from "~/composables/advancedSearchQuery";
import type { AdvancedSearchQuery } from "~/types/advancedSearchQuery";
import { AdvancedSearchModel } from "~/types/advancedSearchModel";

export const useCompoundSearchStore = defineStore(
  "compoundSearchResults",
  () => {
    const loading = ref(false);
    const results = ref<SearchResult>({
      results: [],
    });
    const pageSize = ref(15);
    const currentPage = ref(1);
    const totalRecords = ref(0);
    const appConfig = useAppConfig();
    const maximumSearchResults = Number(appConfig.maximumSearchResults);
    const maxResultsLimitReached = ref(false);
    const totalPages = computed(() => {
      const recordsToDisplay =
        totalRecords.value > maximumSearchResults
          ? maximumSearchResults
          : totalRecords.value;
      return Math.ceil(recordsToDisplay / pageSize.value);
    });
    const searchQuery = ref("");
    const advancedSearchQuery = ref<AdvancedSearchQuery>(
      JSON.parse(JSON.stringify(AdvancedSearchModel)),
    );
    const isAdvancedSearch = ref(false);
    const errorCode = ref(0);
    const apiBaseUrl = useRuntimeConfig().public.apiBaseUrl;
    const resultDownloading = ref(false);

    async function performSearch() {
      try {
        loading.value = true;
        let response;
        const csrfToken = getCookie("csrftoken");
        if (isAdvancedSearch.value) {
          response = await fetch(
            `${apiBaseUrl}/public/advanced_search/?size=${pageSize.value}&page=${currentPage.value}&three_star_only=${advancedSearchQuery.value.threeStarOnly}`,
            {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
                ...(csrfToken ? { "X-CSRFToken": csrfToken } : {}),
              },
              body: JSON.stringify(
                transformSearchQuery(advancedSearchQuery.value),
              ),
            },
          );
        } else {
          response = await fetch(
            `${apiBaseUrl}/public/es_search/?term=${searchQuery.value}&size=${pageSize.value}&page=${currentPage.value}`,
          );
        }
        if (response.ok) {
          const data = await response.json();
          results.value = data as SearchResult;
          totalRecords.value = data.total;
          // UI limits to maximum 20,000 results
          maxResultsLimitReached.value =
            totalRecords.value > maximumSearchResults;
          errorCode.value = 0;
        } else {
          errorCode.value = response.status;
        }
      } catch (error) {
        errorCode.value = 500;
      } finally {
        loading.value = false;
      }
    }

    async function downloadSearchResults() {
      resultDownloading.value = true;
      const csrfToken = getCookie("csrftoken");
      let response;
      if (isAdvancedSearch.value) {
        response = await fetch(
          `${apiBaseUrl}/public/advanced_search/?download=true`,
          {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
              ...(csrfToken ? { "X-CSRFToken": csrfToken } : {}),
            },
            body: JSON.stringify(
              transformSearchQuery(advancedSearchQuery.value),
            ),
          },
        );
      } else {
        response = await fetch(
          `${apiBaseUrl}/public/es_search/?term=${searchQuery.value}&download=true`,
        );
      }
      if (response.ok) {
        const blob = await response.blob();
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", "results.tsv");
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
        window.URL.revokeObjectURL(url);
        resultDownloading.value = false;
      } else {
        resultDownloading.value = false;
        console.error("Error downloading results file");
      }
    }

    function reset_store() {
      pageSize.value = 15;
      currentPage.value = 1;
      totalRecords.value = 0;
      results.value = { results: [] };
      errorCode.value = 0;
    }

    function changePageSize(newValue: number): void {
      pageSize.value = newValue;
      currentPage.value = 1;
      performSearch();
    }

    function changePageNumber(newValue: number): void {
      currentPage.value = newValue;
      performSearch();
    }

    return {
      loading,
      results,
      pageSize,
      currentPage,
      totalRecords,
      maxResultsLimitReached,
      totalPages,
      performSearch,
      reset_store,
      changePageSize,
      changePageNumber,
      searchQuery,
      isAdvancedSearch,
      errorCode,
      advancedSearchQuery,
      downloadSearchResults,
      resultDownloading,
    };
  },
);
