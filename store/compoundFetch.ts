import { defineStore } from "pinia";
import type { CompoundType } from "~/types/compound";

export const useCompoundFetchStore = defineStore("compoundFetch", () => {
  const loading = ref(false);
  const entity = ref<CompoundType>({} as CompoundType);
  const gotEntity = ref(false);
  const notFoundError = ref(false);
  const serverError = ref(false);

  async function fetchCompound(chebiId: any) {
    const runtimeConfig = useRuntimeConfig();
    const apiBaseUrl = runtimeConfig.public.apiBaseUrl;

    try {
      loading.value = true; // Start loading
      const response = await fetch(`${apiBaseUrl}/public/compound/${chebiId}/`);

      if (response.ok) {
        const data = await response.json();
        entity.value = data as CompoundType;
        entity.value.compound_origins = formatOriginsData(
          entity.value.compound_origins,
        );
        gotEntity.value = true;
      } else {
        if (response.status == 404) {
          notFoundError.value = true;
        } else {
          serverError.value = true;
        }
        console.error("API request failed:", response.statusText);
      }
    } catch (error) {
      serverError.value = true;
      console.error("An error occurred:", error);
    } finally {
      loading.value = false; // End loading, regardless of success or failure
    }
  }
  function formatOriginsData(initialData: any) {
    const groupedMap: any = {};
    if (initialData) {
      for (const origin of initialData) {
        const key = `${origin.species_text}`;
        if (!groupedMap?.[key]) {
          groupedMap[key] = {
            species_text: origin.species_text,
            species_accession: origin.species_accession,
            species_accession_url: origin.species_accession_url,
            species_accession_prefix: origin.species_accession_prefix,
            origins: [],
          };
        }
        groupedMap[key].origins.push(origin);
      }
    }
    return groupedMap;
  }

  return {
    loading,
    entity,
    fetchCompound,
    gotEntity,
    notFoundError,
    serverError,
  };
});
